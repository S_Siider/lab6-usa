package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);
    private final DateTimeFormatter pattern = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private final SalesSystemDAO dao;
    private final ShoppingCart cart;

    private String name;
    private String leader;
    private String email;
    private String members;

    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
    }

    public static void main(String[] args) throws Exception {
        SalesSystemDAO dao = new HibernateSalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        Properties properties = new Properties();
        try (InputStream is = this.getClass().getResourceAsStream("/application.properties")) {
            properties.load(is);
            this.name = properties.getProperty("teamname");
            this.leader = properties.getProperty("leader");
            this.email = properties.getProperty("leaderemail");
            this.members = properties.getProperty("members");
        } catch (Exception e) {
            this.name = "N/A";
            this.leader = "N/A";
            this.email = "N/A";
            this.members = "N/A";
        }

        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }
    }

    private void showStock() {
        List<StockItem> stockItems = dao.findStockItems();
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + " Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showCart() {
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si.getName() + " " + si.getPrice() + " Euro (" + si.getQuantity() + " items) Sum: " + si.getTotalSum());
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("Total Sum: " + cart.getSum());
        System.out.println("-------------------------");
    }

    private void showHistoryCart(long idx) {
        System.out.println("-------------------------");
        Purchase purchase = dao.findPurchase(idx);
        if (purchase != null) {
            for (SoldItem si : purchase.getSoldItem()) {
                System.out.println(si.getName() + " " + si.getPrice() + " Euro (" + si.getQuantity() + " items) Sum: " + si.getTotalSum());
            }
            if (purchase.getSoldItem().size() == 0) {
                System.out.println("\tNothing");
            }
            System.out.println("Total Sum: " + purchase.getTotalSum());
        } else {
            System.out.println("Purchase with that IDX doesn't exist");
        }
        System.out.println("-------------------------");
    }

    private void showHistoryCart(String[] c) {
        long idx;
        try {
            idx = Long.parseLong(c[1]);
        } catch (NumberFormatException e) {
            log.error(e);
            System.out.println("ERROR: Can't parse index, make sure index is an integer");
            return;
        }
        showHistoryCart(idx);
    }

    private void showAllHistory() {
        System.out.println("-------------------------");
        for (Purchase p : dao.findPurchases()) {
            System.out.println(p.getPurchase_id() + " " + p.getLocalDate() + " " + p.getTime() + " Total Sum: " + p.getTotalSum());
        }
        if (dao.findPurchases().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void show10History() {
        System.out.println("-------------------------");
        for (Purchase p : dao.findLast10Purchase()) {
            System.out.println(p.getPurchase_id() + " " + p.getLocalDate() + " " + p.getTime() + " Total Sum: " + p.getTotalSum());
        }
        if (dao.findPurchases().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showBetweenHistory(LocalDate start, LocalDate end) {
        System.out.println("-------------------------");
        try {
            for (Purchase p : dao.findBetween(start, end)) {
                System.out.println(p.getPurchase_id() + " " + p.getLocalDate() + " " + p.getTime() + " Total Sum: " + p.getTotalSum());
            }
            if (dao.findPurchases().size() == 0) {
                System.out.println("\tNothing");
            }
        } finally {
            System.out.println("-------------------------");
        }
    }

    private void showTeam() {
        System.out.println("-------------------------");
        System.out.println("Team: " + name);
        System.out.println("Leader: " + leader);
        System.out.println("Leader e-mail: " + email);
        System.out.println("Members: " + members);
        System.out.println("-------------------------");
    }

    private void historyShow(String[] c) {
        LocalDate start;
        LocalDate end;
        try {
            start = LocalDate.parse(c[1], pattern);
            end = LocalDate.parse(c[2], pattern);
        } catch (DateTimeParseException e) {
            log.error(e);
            System.out.println("ERROR: Can't parse given dates, check the format");
            return;
        }
        try {
            showBetweenHistory(start, end);
        } catch (SalesSystemException e) {
            log.error(e);
            System.out.println("ERROR: " + e.getMessage());
        }
    }

    private void addWarehouse(String[] c) {
        try {
            String Name;
            if (c[1].startsWith("\"")) {
                StringBuilder sb = new StringBuilder();
                for (int i = 1; i <= c.length - 3; i++) {
                    sb.append((c[i] + " ").replace("\"", ""));
                }
                Name = sb.toString().trim();
            } else {
                throw new SalesSystemException("Can't parse given inputs! Check that the name is between quotes!");
            }
            String Price = c[c.length - 2];
            String Quantity = c[c.length - 1];
            dao.addToWarehouse(new StockItem(Name, Price, Quantity));
        } catch (SalesSystemException e) {
            log.error(e + ": When trying to parse command: " + Arrays.toString(c));
            System.out.println("ERROR: " + e.getMessage());
        }
    }

    private void deleteCart(String[] c) {
        try {
            long idx = Long.parseLong(c[1]);
            cart.delItem(idx);
        } catch (SalesSystemException | NoSuchElementException e) {
            log.error(e.getMessage(), e);
            System.out.println("ERROR: " + e.getMessage());
        }
    }

    private void addCart(String[] c) {
        try {
            long idx = Long.parseLong(c[1]);
            int amount = Integer.parseInt(c[2]);
            StockItem item = dao.findStockItem(idx);
            if (item != null) {
                cart.addItem(new SoldItem(item, amount));
            } else {
                System.out.println("No stock item with id " + idx);
            }
        } catch (SalesSystemException | NoSuchElementException e) {
            log.error(e.getMessage());
            System.out.println("ERROR: " + e.getMessage());
        } catch (NumberFormatException e) {
            log.error(e.getMessage());
            System.out.println("ERROR: Make sure index and number are integers");
        }
    }

    private void purchaseCart() {
        try {
            if (!cart.getAll().isEmpty()) {
                cart.submitCurrentPurchase();
            } else {
                System.out.println("ERROR: Cart is empty!");
                log.error("submitCurrentPurchase() tried to call out with empty cart");
            }
        } catch (SalesSystemException e) {
            System.out.println("ERROR: " + e.getMessage());
            log.error(e.getMessage(), e);
        }
    }

    private void printUsage() {
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\t:Show this help");
        System.out.println("c\t\t:Show cart contents");
        System.out.println("addc IDX NR \t:Add NR of stock item with index IDX to the cart");
        System.out.println("delc IDX \t:Delete stock item with index IDX from the cart");
        System.out.println("p\t\t:Purchase the shopping cart");
        System.out.println("r\t\t:Reset the shopping cart");
        System.out.println("w\t\t:Show warehouse contents");
        System.out.println("addw \"Name\" P NR \t:Add NR quantity of \"Name\" (write with quotes) to stock with price P");
        System.out.println("hall\t:Show all purchases");
        System.out.println("h10\t\t:Show last 10 purchases");
        System.out.println("hshow S E\t\t:Show purchases between dates S and E (format dd.MM.yyyy)");
        System.out.println("hc IDX\t:Show contents of purchase with index IDX");
        System.out.println("t\t\t:Show team info");
        System.out.println("-------------------------");
    }

    private void processCommand(String command) {
        String[] c = command.split(" ");

        switch (c[0]) {
            case "h":
                printUsage();
                break;
            case "q":
                System.exit(0);
            case "w":
                showStock();
                break;
            case "c":
                showCart();
                break;
            case "p":
                purchaseCart();
                break;
            case "r":
                cart.cancelCurrentPurchase();
                break;
            case "t":
                showTeam();
                break;
            case "addc":
                if (c.length == 3) {
                    addCart(c);
                }
                break;
            case "delc":
                if (c.length == 2) {
                    deleteCart(c);
                }
                break;
            case "addw":
                if (c.length >= 4) {
                    addWarehouse(c);
                }
                break;
            case "hall":
                showAllHistory();
                break;
            case "h10":
                show10History();
                break;
            case "hshow":
                if (c.length == 3) {
                    historyShow(c);
                }
                break;
            case "hc":
                if (c.length == 2) {
                    showHistoryCart(c);
                }
                break;
            default:
                System.out.println("Unknown command!");
                break;
        }
    }
}
