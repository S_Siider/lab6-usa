## Team USA:
1. Urmo Olesk
2. Stiivo Siider
3. Andro Margens
	
## Homework 1:
1. [User Stories](https://bitbucket.org/S_Siider/lab6-usa/wiki/User%20Stories)
2. [Interview Recording](https://www.dropbox.com/s/cgyc8jqro3y05ef/1._intervjuu_kliendiga_18.09.2017%5B1%5D.3gp?dl=0)

## Homework 2:
1. [Use cases, Domain model, Planning](https://bitbucket.org/S_Siider/lab6-usa/wiki/Homework%202:)

## Homework 3:
1. [Tasks](https://bitbucket.org/S_Siider/lab6-usa/wiki/Lab3-tasks)
2. [homework-3](https://bitbucket.org/S_Siider/lab6-usa/commits/tag/homework-3)

## Homework 4:
1. [Debugger questions](https://bitbucket.org/S_Siider/lab6-usa/wiki/Lab%204%20-%20Debugger%20Questions)
2. [homework-4](https://bitbucket.org/S_Siider/lab6-usa/commits/tag/homework-4?page=1)

## Homework 5:
1. [homework-5](https://bitbucket.org/S_Siider/lab6-usa/commits/tag/homework-5)

## Homework 6:
1. [homework-6](https://bitbucket.org/S_Siider/lab6-usa/commits/tag/homework-6)
2. [functional test plan/test cases](https://docs.google.com/spreadsheets/d/1JTmzNBVVSPk0Nfco8QGqfKY-m8g9bKWVmIPoO38z6mY/edit?usp=sharing)

## Homework 7:
1. [Usability testing](https://bitbucket.org/S_Siider/lab6-usa/wiki/Usability%20testing)
2. [Functional test cases](https://docs.google.com/spreadsheets/d/1JTmzNBVVSPk0Nfco8QGqfKY-m8g9bKWVmIPoO38z6mY/edit?usp=sharing)
3. [Test report (Group 1DTT)](https://bitbucket.org/S_Siider/lab6-usa/wiki/Test%20report)
