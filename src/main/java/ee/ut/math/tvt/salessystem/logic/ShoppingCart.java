package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;


public class ShoppingCart {

    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();
    private static final Logger log = LogManager.getLogger(ShoppingCart.class);

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }


    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {
        StockItem stockItem = dao.findStockItem(item.getStockId());

        for (SoldItem elem : items) {
            if (elem.getStockId() == item.getStockId()) {
                if (stockItem.getQuantity() - elem.getQuantity() - item.getQuantity() >= 0) {
                    int oldQuantity = elem.getQuantity();
                    elem.setQuantity(elem.getQuantity() + item.getQuantity());
                    elem.updateSum();
                    log.info("Changed " + item.getName() + " quantity from " + oldQuantity + " to " + elem.getQuantity());
                    return;
                } else {
                    log.error("Tried to add too much of item " + elem);
                    throw new SalesSystemException("Warehouse doesn't have that many of item: " + stockItem);
                }
            }
        }
        if (stockItem.getQuantity() - item.getQuantity() >= 0) {
            items.add(item);
            log.info("Added " + item.getName() + " quantity of " + item.getQuantity());
        } else {
            log.error("Warehouse doesn't have that much of item " + stockItem);
            throw new SalesSystemException("Warehouse doesn't have that many of item: " + stockItem);
        }
    }

    public void delItem(long id) {
        for (SoldItem item : items) {
            if (item.getStockId() == id) {
                items.remove(item);
                log.info("Removed " + item.getName() + " from shopping cart.");
                return;
            }
        }
        log.info("No such item in shopping cart");
    }

    public List<SoldItem> getAll() {
        return items;
    }

    //Calculates the shopping cart sum.
    public double getSum() {
        double sum = 0;
        for (SoldItem elem : items) {
            sum += elem.getTotalSum();
        }
        return sum;
    }

    public void cancelCurrentPurchase() {
        items.clear();
    }

    public void submitCurrentPurchase() {
        dao.beginTransaction();
        try {
            for (SoldItem soldItem : items) {
                StockItem stockItem = dao.findStockItem(soldItem.getStockId());
                if (stockItem.getQuantity() - soldItem.getQuantity() == 0) {
                    dao.deleteStockItem(stockItem);
                } else {
                    stockItem.setQuantity(stockItem.getQuantity() - soldItem.getQuantity());
                }
                dao.saveSoldItem(soldItem);
            }
            LocalDate localDate = LocalDate.now();
            LocalTime localTime = ZonedDateTime.now().toLocalTime().truncatedTo(ChronoUnit.SECONDS);
            Purchase purchase = new Purchase(new ArrayList<>(items), localDate, localTime, getSum());
            dao.savePurchase(purchase);
            dao.commitTransaction();
            log.info(purchase);
            items.clear();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw new SalesSystemException(e + " Transaction went wrong!");
        }
    }
}
