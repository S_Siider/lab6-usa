package ee.ut.math.tvt.salessystem.dataobjects;


import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.*;

/**
 * Already bought StockItem. SoldItem duplicates name and price for preserving history.
 */
@Entity
public class SoldItem {
    @Transient
    static final Logger log = LogManager.getLogger(InMemorySalesSystemDAO.class);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long stockId;
    @ManyToOne
    @JoinColumn(name = "purchase_id")
    private Purchase purchase;
    private String name;
    private int quantity;
    private double price;
    private double totalSum;

    public SoldItem() {
    }

    public SoldItem(StockItem stockItem, int quantity) {
        if (quantity < 0) {
            log.error("Quantity can't be negative!");
            throw new SalesSystemException("Quantity can't be negative!");
        }
        this.stockId = stockItem.getId();
        this.name = stockItem.getName();
        this.price = stockItem.getPrice();
        this.quantity = quantity;
        this.totalSum = ((int) (price * quantity * 100)) / 100.0;
    }

    public long getId() {
        return id;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    public long getStockId() {
        return stockId;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void updateSum() {
        this.totalSum = ((int) (price * quantity * 100)) / 100.0;
    }

    @Override
    public String toString() {
        return String.format("SoldItem{id=%d, name='%s'}", id, name);
    }
}
