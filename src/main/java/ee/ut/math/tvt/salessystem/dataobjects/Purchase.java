package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Entity
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long purchase_id;
    @OneToMany(mappedBy = "purchase")
    private List<SoldItem> soldItem;
    private LocalDate localDate;
    private LocalTime time;
    private double totalSum;

    public Purchase() {
    }

    public Purchase(List<SoldItem> soldItem, LocalDate localDate, LocalTime time, double totalSum) {
        this.soldItem = soldItem;
        this.localDate = localDate;
        this.time = time;
        this.totalSum = totalSum;
    }

    public List<SoldItem> getSoldItem() {
        return soldItem;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public LocalTime getTime() {
        return time;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public long getPurchase_id() {
        return purchase_id;
    }

    public void setPurchase_id(long purchase_id) {
        this.purchase_id = purchase_id;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "soldItem=" + soldItem +
                ", date=" + localDate +
                ", time=" + time +
                '}';
    }
}
