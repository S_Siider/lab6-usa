package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public List<StockItem> findStockItems() {
        TypedQuery<StockItem> tQuery = em.createQuery("select e from StockItem e", StockItem.class);
        return tQuery.getResultList();
    }

    @Override
    public StockItem findStockItem(long id) {
        return em.find(StockItem.class, id);
    }

    @Override
    public void deleteStockItem(StockItem item) {
        em.remove(item);
    }

    @Override
    public List<Purchase> findPurchases() {
        TypedQuery<Purchase> tQuery = em.createQuery("select e from Purchase e order by localDate desc, time desc", Purchase.class);
        return tQuery.getResultList();
    }

    @Override
    public Purchase findPurchase(long id) {
        return em.find(Purchase.class, id);
    }

    @Override
    public List<Purchase> findLast10Purchase() {
        TypedQuery<Purchase> tQuery = em.createQuery("select e from Purchase e order by localDate desc, time desc", Purchase.class).setMaxResults(10);
        return tQuery.getResultList();
    }

    @Override
    public List<Purchase> findBetween(LocalDate start, LocalDate end) {
        if (end.isBefore(start) || start.isAfter(end)) {
            throw new SalesSystemException("End Date is after Start Date");
        }
        TypedQuery<Purchase> tQuery = em.createQuery("select e from Purchase e where localDate between :startdate and :enddate order by localDate desc, time desc", Purchase.class).setParameter("startdate", start).setParameter("enddate", end);
        return tQuery.getResultList();
    }

    @Override
    public List<SoldItem> findPurchaseSoldItems(Purchase purchase) {
        TypedQuery<SoldItem> tQuery = em.createQuery("select e from SoldItem e, Purchase t where e.purchase.purchase_id = t.purchase_id and :purchased = t ", SoldItem.class).setParameter("purchased", purchase);
        return tQuery.getResultList();
    }

    @Override
    public long getLastStockID() {
        return 0;
    }

    @Override
    public long getLastPurchaseID() {
        return 0;
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        em.persist(stockItem);
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        em.persist(item);
    }

    @Override
    public void savePurchase(Purchase purchase) {
        for (SoldItem item : purchase.getSoldItem()) {
            SoldItem temp = em.find(SoldItem.class, item.getId());
            temp.setPurchase(purchase);
        }
        em.persist(purchase);
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }

    @Override
    public void addToWarehouse(StockItem stockItem) {
        beginTransaction();
        try {
            TypedQuery<StockItem> tQuery = em.createQuery("select e from StockItem e where LOWER(e.name) = :name", StockItem.class).setParameter("name", stockItem.getName().toLowerCase());
            List<StockItem> queryResult = tQuery.getResultList();
            if (!queryResult.isEmpty()) {
                queryResult.get(0).alterStockItem(stockItem.getPrice(), stockItem.getQuantity());
                commitTransaction();
                return;
            }
            saveStockItem(stockItem);
            commitTransaction();
            log.info(stockItem + " added to stock.");
        } catch (Exception e) {
            rollbackTransaction();
            throw new SalesSystemException(e.getMessage());
        }
    }
}
