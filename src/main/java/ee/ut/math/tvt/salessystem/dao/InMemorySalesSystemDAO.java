package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<Purchase> purchaseList;
    private long lastStockId;
    private long lastPurchaseId;

    private boolean commitTransactionCalled = false;
    private boolean beginTransactionCalled = false;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<>();
        this.lastStockId = 0L;
        this.lastPurchaseId = 0L;
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.purchaseList = new ArrayList<>();
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public void deleteStockItem(StockItem item) {
        stockItemList.remove(item);
    }

    @Override
    public List<Purchase> findPurchases() {
        return purchaseList;
    }

    @Override
    public Purchase findPurchase(long id) {
        for (Purchase item : purchaseList) {
            if (item.getPurchase_id() == id)
                return item;
        }
        return null;
    }

    @Override
    public List<Purchase> findLast10Purchase() {
        List<Purchase> purchases = findPurchases();
        int length = purchases.size();
        return purchases.subList((length - 10 < 0 ? 0 : length - 10), length);
    }

    @Override
    public List<Purchase> findBetween(LocalDate start, LocalDate end) {
        if (end.isBefore(start) || start.isAfter(end)) {
            throw new SalesSystemException("End Date is after Start Date");
        }
        List<Purchase> between = new ArrayList<>();
        for (Purchase purchase : purchaseList)
            if (!purchase.getLocalDate().isBefore(start) && !purchase.getLocalDate().isAfter(end)) {
                between.add(purchase);
            }
        return between;
    }

    @Override
    public List<SoldItem> findPurchaseSoldItems(Purchase purchase) {
        return purchase.getSoldItem();
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        stockItem.setId(++lastStockId);
        stockItemList.add(stockItem);
    }

    @Override
    public void savePurchase(Purchase purchase) {
        purchase.setPurchase_id(++lastPurchaseId);
        purchaseList.add(purchase);
    }

    @Override
    public long getLastStockID() {
        return lastStockId;
    }

    @Override
    public long getLastPurchaseID() {
        return lastPurchaseId;
    }

    @Override
    public void beginTransaction() {
        /*if (beginTransactionCalled) {
            throw new SalesSystemException("Begin has already been called");
        }
        beginTransactionCalled = true;*/
    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
        /*if (!beginTransactionCalled) {
            throw new SalesSystemException("Commit called before Begin");
        } else if (commitTransactionCalled) {
            throw new SalesSystemException("Commit has already been called");
        } else {
            commitTransactionCalled = true;
        }*/
    }

    public void resetTransaction(){
/*        beginTransactionCalled = false;
        commitTransactionCalled = false;*/
    }

    @Override
    public void addToWarehouse(StockItem stockItem) {
        beginTransaction();
        try {
            for (StockItem elem : findStockItems()) {
                if (stockItem.getName().toLowerCase().equals(elem.getName().toLowerCase())) {
                    elem.alterStockItem(stockItem.getPrice(), stockItem.getQuantity());
                    commitTransaction();
                    resetTransaction();
                    return;
                }
            }
            saveStockItem(stockItem);
            commitTransaction();
            resetTransaction();
            log.info(stockItem + " added to stock.");
        } catch (Exception e) {
            rollbackTransaction();
            resetTransaction();
            throw new SalesSystemException(e.getMessage());
        }
    }
}
