package ee.ut.math.tvt.salessystem.dataobjects;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * Stock item.
 */
@Entity
public class StockItem {
    @Transient
    static final Logger log = LogManager.getLogger(InMemorySalesSystemDAO.class);

    @Id
    @GenericGenerator(name = "custom_gen", strategy = "ee.ut.math.tvt.salessystem.logic.CustomIdGenerator")
    @GeneratedValue(generator = "custom_gen")
    private long id;
    private String name;
    private double price;
    private int quantity;

    public StockItem() {
    }

    public StockItem(String name, double price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        if (price < 0 || quantity < 0) {
            throw new SalesSystemException("Price/Quantity field can't be negative!");
        }
        this.quantity = this.quantity == 0 ? 1 : this.quantity;
    }

    public StockItem(String Name, String Price, String Quantity) {
        this.name = Name;
        try {
            this.price = ((int) (100 * Double.parseDouble(Price))) / 100.0;
            this.quantity = Integer.parseInt(Quantity);
            if (price < 0 || quantity < 0) {
                throw new SalesSystemException("Price/Quantity field can't be negative!");
            }
            quantity = quantity == 0 ? 1 : quantity;
        } catch (NumberFormatException e) {
            throw new SalesSystemException("Can't parse given inputs! Check that the Price(= " + Price + ") and Quantity(= " + Quantity + ") fields are numbers!");
        } catch (Exception e) {
            throw new SalesSystemException(e.getMessage());
        }
    }

    public void alterStockItem(double price, int quantity) {
        if (price != this.price) {
            double oldPrice = this.price;
            this.price = price;
            log.info(name + " price changed from " + oldPrice + " to " + this.price);
        }
        if (quantity != 0) {
            int oldQuantity = this.quantity;
            this.quantity = quantity + oldQuantity;
            log.info(name + " quantity changed from " + oldQuantity + " to " + this.quantity);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return String.format("StockItem{id=%d, name='%s'}", id, name);
    }
}
