package ee.ut.math.tvt.salessystem.logic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomIdGenerator implements IdentifierGenerator {

    private static final Logger log = LogManager.getLogger(ShoppingCart.class);

    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        Connection connection = session.connection();
        try {
            ResultSet rs = connection.prepareStatement("select * from StockItem").executeQuery();
            List<Long> ls = new ArrayList<>();
            boolean b = true;
            while (rs.next()) {
                b = false;
                ls.add(rs.getLong("id"));
            }
            if (b) {
                return (long) 1;
            }
            Collections.sort(ls);
            if (ls.size() == Collections.max(ls)) {
                return (long) (ls.size() + 1);
            } else {
                for (int i = 1; i <= ls.size(); i++) {
                    if (i != ls.get(i - 1)) {
                        return (long) i;
                    }
                }
            }
        } catch (SQLException e) {
            log.error(e);
            throw new HibernateException("Unable to generate identifier");
        }
        return null;
    }
}
