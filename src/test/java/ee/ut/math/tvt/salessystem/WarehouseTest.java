package ee.ut.math.tvt.salessystem;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class WarehouseTest {
    private InMemorySalesSystemDAO dao;
    private StockItem item1;
    private StockItem item2;

    @Before
    public void setUp() {
        dao = new InMemorySalesSystemDAO();
        dao.resetTransaction();
        item1 = new StockItem("Bread", 1, 5);
        item2 = new StockItem("Bread", 2, 5);
    }

    @Test
    public void testAddingItemBeginsAndCommitsTransaction() {
        try {
            dao.addToWarehouse(item1);
        } catch (SalesSystemException e) {
            fail("Fault in transactions");
        }
    }

    @Test
    public void testAddingNewItem() {
        dao.addToWarehouse(item1);
        assertEquals(dao.findStockItems().get(0).getPrice(), 1, 0.001);
        assertEquals(dao.findStockItems().get(0).getQuantity(), 5);
    }

    @Test
    public void testAddingExistingItem() {
        dao.addToWarehouse(item1);
        dao.addToWarehouse(item2);
        assertEquals(dao.findStockItems().size(), 1);
        assertEquals(dao.findStockItems().get(0).getPrice(), 2, 0.001);
        assertEquals(dao.findStockItems().get(0).getQuantity(), 10);
    }

    @Test
    public void testAddingItemWithNegativeQuantity() {
        try {
            StockItem item3 = new StockItem("Bread", 1, -1);
            dao.addToWarehouse(item3);
            fail("Failed to throw exception with negative quantity");
        } catch (Exception e) {
            assertEquals(dao.findStockItems().size(), 0);
        }
    }
}
