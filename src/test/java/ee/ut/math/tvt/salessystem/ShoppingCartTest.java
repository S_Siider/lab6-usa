package ee.ut.math.tvt.salessystem;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


public class ShoppingCartTest {
    private ShoppingCart sc;
    private InMemorySalesSystemDAO dao;
    private StockItem item1;

    @Before
    public void setUp() {
        dao = new InMemorySalesSystemDAO();
        dao.resetTransaction();
        sc = new ShoppingCart(dao);
        item1 = new StockItem("Bread", 1, 5);
        dao.saveStockItem(item1);
    }

    //Tests for adding new items
    @Test
    public void testAddingNewItem() {
        sc.addItem(new SoldItem(item1, 2));
        assertEquals(sc.getAll().size(), 1);
        assertEquals(sc.getAll().get(0).getQuantity(), 2);
        assertEquals(dao.findStockItems().size(), 1);
        assertEquals(dao.findStockItems().get(0).getQuantity(), 5);
    }

    @Test
    public void testAddingExistingItem() {
        sc.addItem(new SoldItem(item1, 2));
        sc.addItem(new SoldItem(item1, 3));
        assertEquals(sc.getAll().size(), 1);
        assertEquals(sc.getAll().get(0).getQuantity(), 5);
        assertEquals(dao.findStockItems().size(), 1);
        assertEquals(dao.findStockItems().get(0).getQuantity(), 5);
    }

    @Test
    public void testAddingItemWithNegativeQuantity() {
        try {
            sc.addItem(new SoldItem(item1, -1));
            fail("Failed to throw exception with negative quantity");
        } catch (SalesSystemException e) {
            assertEquals(sc.getAll().size(), 0);
            assertEquals(dao.findStockItems().size(), 1);
            assertEquals(dao.findStockItems().get(0).getQuantity(), 5);
        }
    }

    @Test
    public void testAddingItemWithQuantityTooLarge() {
        try {
            sc.addItem(new SoldItem(item1, 6));
            fail("Failed to throw exception with too large quantity");
        } catch (SalesSystemException e) {
            assertEquals(sc.getAll().size(), 0);
            assertEquals(dao.findStockItems().size(), 1);
            assertEquals(dao.findStockItems().get(0).getQuantity(), 5);
        }
    }

    @Test
    public void testAddingItemWithQuantitySumTooLarge() {
        sc.addItem(new SoldItem(item1, 3));
        try {
            sc.addItem(new SoldItem(item1, 3));
            fail("Failed to throw exception with too large sum quantity");
        } catch (SalesSystemException e) {
            assertEquals(sc.getAll().size(), 1);
            assertEquals(sc.getAll().get(0).getQuantity(), 3);
            assertEquals(dao.findStockItems().size(), 1);
            assertEquals(dao.findStockItems().get(0).getQuantity(), 5);
        }
    }

    //Tests for submiting purchase
    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() {
        sc.addItem(new SoldItem(item1, 2));
        sc.submitCurrentPurchase();
        assertEquals(sc.getAll().size(), 0);
        assertEquals(dao.findStockItems().size(), 1);
        assertEquals(dao.findStockItems().get(0).getQuantity(), 3);
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() {
        try {
            sc.addItem(new SoldItem(item1, 2));
            sc.submitCurrentPurchase();
        } catch (SalesSystemException e) {
            fail("Fault in transactions");
        }
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem() {
        sc.addItem(new SoldItem(item1, 2));
        sc.submitCurrentPurchase();
        assertEquals(dao.findPurchases().size(), 1);
        assertEquals(dao.findPurchases().get(0).getSoldItem().size(), 1);
        assertEquals(dao.findPurchases().get(0).getSoldItem().get(0).getQuantity(), 2);

    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() {
        sc.addItem(new SoldItem(item1, 2));
        sc.submitCurrentPurchase();
        assertEquals(dao.findPurchases().get(0).getLocalDate(), LocalDate.now());
        assertEquals(dao.findPurchases().get(0).getTime().truncatedTo(ChronoUnit.MINUTES), ZonedDateTime.now().toLocalTime().truncatedTo(ChronoUnit.MINUTES));
    }

    @Test
    public void testCancellingOrder() {
        sc.addItem(new SoldItem(item1, 2));
        sc.cancelCurrentPurchase();
        assertEquals(dao.findPurchases().size(), 0);
        assertEquals(sc.getAll().size(), 0);
    }

    @Test
    public void testCancellingOrderQuanititesUnchanged() {
        sc.addItem(new SoldItem(item1, 2));
        sc.cancelCurrentPurchase();
        assertEquals(dao.findStockItems().size(), 1);
        assertEquals(dao.findStockItems().get(0).getQuantity(), 5);
    }
}
