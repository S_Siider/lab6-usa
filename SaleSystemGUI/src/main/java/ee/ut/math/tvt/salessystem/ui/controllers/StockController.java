package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.net.URL;
import java.util.ResourceBundle;

public class StockController implements Initializable {
    private static final Logger log = LogManager.getLogger(StockController.class);

    final SalesSystemDAO dao;

    @FXML
    private Button addItem;
    @FXML
    private TableView<StockItem> warehouseTableView;
    @FXML
    private TextField nameField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField priceField;


    public StockController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();
    }

    @FXML
    public void addItemButtonClicked() {
        log.info("Adding item to warehouse");
        if (!nameField.getText().trim().isEmpty()) {
            String Name = nameField.getText();
            String Price = priceField.getText();
            String Quantity = quantityField.getText();
            if (Quantity.equals("")) {
                Quantity = "0";
            }
            //String Description = insertDescription.getText();
            try {
                dao.addToWarehouse(new StockItem(Name, Price, Quantity));
            } catch (SalesSystemException e) {
                log.error(e + ": When trying to add item {Name = " + Name + "; Price = " + Price + "; Quantity = " + Quantity);
                JOptionPane.showMessageDialog(null, e.getMessage(), "Error message", JOptionPane.ERROR_MESSAGE);
            }
            refreshStockItems();
        }
    }

    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
    }

    private void refreshStockItems() {
        warehouseTableView.setItems(new ObservableListWrapper<>(dao.findStockItems()));
        warehouseTableView.refresh();
    }
}
