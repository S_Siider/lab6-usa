package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class PurchaseController implements Initializable {

    private static final Logger log = LogManager.getLogger(PurchaseController.class);

    final SalesSystemDAO dao;

    private final ShoppingCart shoppingCart;

    @FXML
    private Button newPurchase;
    @FXML
    private Button submitPurchase;
    @FXML
    private Button cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItemButton;
    @FXML
    private TableView<SoldItem> purchaseTableView;
    @FXML
    private Button deleteButton;
    @FXML
    private Label sumLabel;

    public PurchaseController(SalesSystemDAO dao, ShoppingCart shoppingCart) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        nameField.setEditable(false);
        priceField.setEditable(false);
        purchaseTableView.setItems(new ObservableListWrapper<>(shoppingCart.getAll()));
        disableProductField(true);

        deleteButton.setOnAction((event) -> {
            SoldItem selectedItem = purchaseTableView.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                purchaseTableView.getItems().remove(selectedItem);
                this.sumLabel.setText("Sum: " + String.valueOf(shoppingCart.getSum()));
                this.sumLabel.setTextFill(Color.RED);
                log.info(selectedItem.getName() + " removed from shopping cart.");
            }
        });

        barCodeField.setOnKeyPressed((event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                fillInputsBySelectedStockItem();
            }
        });

        barCodeField.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue) {
                fillInputsBySelectedStockItem();
            }
        });


    }

    /**
     * Event handler for the <code>new purchase</code> event.
     */
    @FXML
    protected void newPurchaseButtonClicked() {
        log.info("New sale process started");
        try {
            enableInputs();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    protected void cancelPurchaseButtonClicked() {
        log.info("Sale cancelled");
        try {
            shoppingCart.cancelCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     */
    @FXML
    protected void submitPurchaseButtonClicked() {
        if (shoppingCart.getAll().isEmpty()) {
            log.error("Tried to submit empty purchase");
            JOptionPane.showMessageDialog(null, "Shopping cart is empty! Can't submit empty purchase!", "Error message", JOptionPane.ERROR_MESSAGE);
            return;
        }
        log.info("Sale complete");
        try {
            log.debug("Contents of the current basket:\n" + shoppingCart.getAll());
            //This is where we find shopping cart sum and put to label.
            this.sumLabel.setText("Sum: " + String.valueOf(shoppingCart.getSum()));
            this.sumLabel.setTextFill(Color.RED);
            log.info("Shopping cart sum is: " + shoppingCart.getSum());
            shoppingCart.submitCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error message", JOptionPane.ERROR_MESSAGE);
            log.error(e.getMessage(), e);
        }
    }

    // switch UI to the state that allows to proceed with the purchase
    private void enableInputs() {
        resetProductField();
        disableProductField(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
        sumLabel.setText("");
    }

    // switch UI to the state that allows to initiate new purchase
    private void disableInputs() {
        resetProductField();
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
    }


    /**
     * Add new item to the cart.
     */
    @FXML
    public void addItemEventHandler() {
        // add chosen item to the shopping cart.
        log.debug("Adding item to cart");
        StockItem stockItem = getStockItemByBarcode();
        if (stockItem != null) {
            int quantity;
            try {
                quantity = Integer.parseInt(quantityField.getText());
                shoppingCart.addItem(new SoldItem(stockItem, quantity));
                this.sumLabel.setText("Sum: " + String.valueOf(shoppingCart.getSum()));
                this.sumLabel.setTextFill(Color.RED);
            } catch (NumberFormatException e) {
                log.error("Can't parse quantity input! Make sure quantity is an integer");
                JOptionPane.showMessageDialog(null, "Can't parse quantity input! Make sure quantity is an integer", "Error message", JOptionPane.ERROR_MESSAGE);
            } catch (SalesSystemException e) {
                log.error(e.getMessage() + ": When trying to add item " + stockItem);
                JOptionPane.showMessageDialog(null, e.getMessage(), "Error message", JOptionPane.ERROR_MESSAGE);
            }
            purchaseTableView.setItems(new ObservableListWrapper<>(shoppingCart.getAll()));
            purchaseTableView.refresh();
        }
    }

    /**
     * Sets whether or not the product component is enabled.
     */
    private void disableProductField(boolean disable) {
        addItemButton.setDisable(disable);
        deleteButton.setDisable(disable);
        barCodeField.setDisable(disable);
        quantityField.setDisable(disable);
        nameField.setDisable(disable);
        priceField.setDisable(disable);
    }

    private void fillInputsBySelectedStockItem() {
        StockItem stockItem = getStockItemByBarcode();
        if (stockItem != null) {
            nameField.setText(stockItem.getName());
            priceField.setText(String.valueOf(stockItem.getPrice()));
        } else {
            resetProductField();
        }
    }

    // Search the warehouse for a StockItem with the bar code entered
    // to the barCodeField textfield.
    private StockItem getStockItemByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private void resetProductField() {
        barCodeField.setText("");
        quantityField.setText("");
        nameField.setText("");
        priceField.setText("");
    }
}
