package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Purchase;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {
    private static final Logger log = LogManager.getLogger(HistoryController.class);

    final SalesSystemDAO dao;

    @FXML
    private Button showBetweenButton;
    @FXML
    private Button show10Button;
    @FXML
    private Button showAllButton;
    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;
    @FXML
    private TableView<Purchase> historyTableView;
    @FXML
    private TableView<SoldItem> purchaseHistoryTableView;

    public HistoryController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        historyTableView.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && !historyTableView.getSelectionModel().isEmpty()) {
                purchaseHistoryTableView.setItems(new ObservableListWrapper<>(dao.findPurchaseSoldItems(historyTableView.getSelectionModel().getSelectedItem())));
                purchaseHistoryTableView.refresh();
            }
        });
    }

    @FXML
    protected void showAllButtonClicked() {
        historyTableView.setItems(new ObservableListWrapper<>(dao.findPurchases()));
        historyTableView.refresh();
    }

    @FXML
    protected void showBetweenButtonClicked() {
        if (startDate.getValue() == null || endDate.getValue() == null) {
            JOptionPane.showMessageDialog(null, "Atleast one of the dates has not been entered", "Warning Message", JOptionPane.WARNING_MESSAGE);
            return;
        }
        LocalDate start = startDate.getValue();
        LocalDate end = endDate.getValue();
        List<Purchase> between = new ArrayList<>();
        try {
            between = dao.findBetween(start, end);
        } catch (SalesSystemException e) {
            log.error(e);
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error Message", JOptionPane.ERROR_MESSAGE);
        }
        historyTableView.setItems(new ObservableListWrapper<>(between));
        historyTableView.refresh();
    }

    @FXML
    protected void show10ButtonClicked() {
        historyTableView.setItems(new ObservableListWrapper<>(dao.findLast10Purchase()));
        historyTableView.refresh();
    }
}
