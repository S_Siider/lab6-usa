package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

//Labelled "Team" in the menu, contains team information
//http://www.baeldung.com/java-properties
public class TeamController implements Initializable {
    private final Properties properties = new Properties();
    private static final Logger log = LogManager.getLogger(TeamController.class);
    @FXML
    private Text name;
    @FXML
    private Text leader;
    @FXML
    private Text email;
    @FXML
    private Text members;
    @FXML
    private ImageView logo;

    public TeamController() throws IOException {
        try (InputStream is = this.getClass().getResourceAsStream("/application.properties")) {
            if (is != null) {
                properties.load(is);
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Text[] temp1 = {name, leader, email, members};
        String[] temp2 = {"teamname", "leader", "leaderemail", "members"};
        for (int i = 0; i < temp1.length; i++) {
            if (properties.getProperty(temp2[i]) == null) {
                temp1[i].setText("N/A");
            } else {
                temp1[i].setText(properties.getProperty(temp2[i]));
            }
        }

        if (properties.getProperty("logo") == null) {
            logo = new ImageView();
        } else {
            logo.setImage(new Image(this.properties.getProperty("logo")));
        }
    }
}
